# `byakhee.pl`

## What is this

This script is update checker for web pages.

## How to use

   $ ./byakhee.pl --config=config.yaml

Please see source code about more options.

# `shoggoth.pl`

## What is this

This script templatizer for `byakhee.pl` output yaml file.

## How to use

   $ ./shoggoth.pl --templates-dir=templates --template=latest.html --variables=updated.yaml

Plese see source code about more options.

# FAQ

## What license are these ?

The license of these scripts is same as Perl.

## Why are documents too simple ?

It's because I made only my purpose.

# Author

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net*

WebSites (Japanese):

* [Twitter: @nyarla](https://twitter.com/nyarla)
* [Facebook: Naoki Okamura](http://www.facebook.com/profile.php?id=100001722887797)
* [空繰再繰 (Blog)](http://blog.nyarla.net/)
* [無貌断片 (Diary)](http://diary.nyarla.net/)

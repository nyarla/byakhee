#!/usr/bin/env perl

use strict;
use warnings;

our $VERSION = 0.01;

use Getopt::Long;

use Path::Class;
use FindBin ();

use YAML::Syck;
use Text::MicroTemplate::Extended;

my $bindir   = dir( $FindBin::Bin )->absolute->cleanup;
my $tmpldir  = $bindir->subdir('templates')->stringify;
my $template = 'latest.html';
my $varsfile = $bindir->file('updated.yaml')->stringify;
my $output   = $bindir->file($template)->stringify;

GetOptions(
    'templates-dir=s' => \$tmpldir,
    'template=s'      => \$template,
    'variables=s'     => \$varsfile,
    'output=s'        => \$output,
    ) or die "Usage: ${0} --templates-dir=template --template=latest.html --variables=updated.yaml";

my $vars    = YAML::Syck::LoadFile( $varsfile );

my $renderer = Text::MicroTemplate::Extended->new(
    include_path  => [ $tmpldir ],
    extension     => '',
    template_args => {
        vars => sub { $vars },
    },
    macro         => {
        encoded_string => sub ($) { Text::MicroTemplate::encoded_string(@_) },
    },
);

my $fh = file($output)->openw;
print $fh $renderer->render($template);
$fh->close;

exit;

=head1 NAME

shoggoth.pl - Templatizer for byakhee output file.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This application is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut

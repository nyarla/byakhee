#!/usr/bin/env perl

package Byakhee;

use strict;
use warnings;

our $VERSION = 0.01;

use Getopt::Long;
use Log::Minimal;

use Path::Class;
use FindBin;
use File::Basename;
use YAML::Syck;
use Cache::FileCache;

use URI;
use Furl;
use HTTP::Date;
use Coro;
use Coro::Select;
use Text::Diff qw/diff/;

# default valoue

my $agent       = "Byakhee/${VERSION}";
my $confile     = file( $FindBin::Bin, 'byakhee.yaml' )->absolute->cleanup->stringify;
my $updatedfile = file( $FindBin::Bin, 'updated.yaml' )->absolute->cleanup->stringify;
my $failedfile  = file( $FindBin::Bin, 'failed.yaml' )->absolute->cleanup->stringify; 

# parse options
GetOptions(
    'config=s'  => \$confile,
    'updated-output=s'  => \$updatedfile,
    'failed-output=s'  => \$failedfile,
    'v|version' => sub { print "${agent}\n"; exit; },
    'h|help'    => sub { print "Usage: ${0} --config=config.yaml\n"; exit; },
    ) or die "Usage: ${0} --config=yaml";

my $config   = YAML::Syck::LoadFile( $confile );

# setup system variables
my $appdir   = dir( dirname(__FILE__) )->absolute->cleanup;
my $bindir   = dir($FindBin::Bin)->absolute->cleanup;

my $cachedir = ( exists $config->{'cachedir'} ) ? dir( $config->{'cachedir'} ) : $appdir->subdir('.cache');
   $cachedir->mkpath if ( ! -d $cachedir );
my $cache    = Cache::FileCache->new({ cache_root => $cachedir->stringify, cache_depth => 3 });

my $filters = $config->{'filters'} || [];

# prepare request
my %hosts = ();

for my $request ( @{ $config->{'source'} } ) {
    # Str => HashRef
    $request = { uri => $request } if ( ! ref $request );

    # Validate
    die "Request URI is not specified: @{[ YAML::Syck::Dump($request) ]}"
        if ( ! exists $request->{'uri'} );

    # alias
    $request->{'link'} ||= $request->{'uri'};

    # push request to host task
    my $host = URI->new($request->{'uri'})->host;
    $hosts{$host} ||= [];
    push @{ $hosts{$host} }, $request; 
}

# setup tasks
my @tasks   = ();
my @updated = ();
my @failed  = ();

for my $host ( sort keys %hosts ) {
    my $tasks = $hosts{$host};

    push @tasks, async {
        infof('[Start] %s', $host);

      REQUEST: for my $request ( @{ $tasks } ) {
          my $uri = $request->{'uri'};
          
          debugf('[Fetch] %s', $uri);

          my $furl = Furl->new( agent => $agent, timeout => 10 );
          my $res  = $furl->get($uri);

          if ( $res->is_success ) {
              # logging
              infof('[Success] %s', $uri);

              # setup site variables
              my $content   = $res->as_http_response->decoded_content; 
              my ( $title ) = ( $content =~ m{<title.*?>(.+?)</title>}si );
              my $lastmod   = eval { HTTP::Date::str2time( $res->headers->last_modified ) } || undef;
              my $checktime = time;

              my $before    = $cache->get($uri);
              my $site      = { uri => $uri, link => $request->{'link'}, title => $title, updated => ( defined $lastmod ? $lastmod : $checktime ), };
              my $metacache = { lastcheck => $checktime, lastmod => $lastmod, content => $content  };

              # first time
              if ( ! $before ) {
                  push @updated, $site;
                  $cache->set( $uri => $metacache );
              }
              # after
              else {
                  my $before_lastmod = ( exists $before->{'lastmod'} ) ? $before->{'lastmod'} : undef;
                  my $before_check   = $before->{'lastcheck'};

                  # now defined, before: defined
                  if ( defined $lastmod && defined $before_lastmod ) {
                      if ( $lastmod > $before_lastmod ) {
                          push @updated, $site;
                          $cache->set( $uri => $metacache );
                      }
                  }
                  # now: defined, before: undefined
                  elsif ( defined $lastmod && ! defined $before_lastmod ) {
                      if ( $lastmod > $before_check ) {
                          push @updated, $site;
                          $cache->set( $uri => $metacache  );
                      }
                  }
                  # now: undefined, before: defined
                  elsif ( ! defined $lastmod && defined $before_lastmod ) {
                      push @updated, $site;
                      $cache->set( $uri => $metacache )
                  }
                  # now: undefined, before: undefined
                  else {
                      my $target_A = $before->{'content'};
                      my $target_B = $content;

                      # filter
                      for my $source ( $target_A, $target_B ) {
                          for my $filter ( @{ $request->{'filter'} || [] } ) {
                              $source =~ s/${filter}//gsi;
                          }

                          for my $filter ( @{ $filters } ) {
                              $filter = { filter => $filter } if ( ! ref $filter );
                              $filter->{'url'}   ||= '.*?';
                              $filter->{'filter'} ||= q{};

                              if ( $uri =~ m/$filter->{'uri'}/ ) {
                                  $source =~ s/$filter->{'filter'}//gsi;
                              }
                          }
                      }
                      
                      my $diff = diff( \$target_A, \$target_B );

                      if ( !! $diff ) {
                          $site->{'diff'} = $diff;
                          push @updated, $site;
                          $cache->set( $uri => $metacache );
                      }
                  }
              }


          }
          else {
              infof('[Failed] %s', $uri);
              push @failed, +{
                  request => $request,
                  status  => $res->status_line,
              };
          }

      }
    };
}

# crawle
$_->join for @tasks;

# output
infof('[Saveing Results] updated');
YAML::Syck::DumpFile( $updatedfile,  \@updated );

infof('[Saving Results] failed');
YAML::Syck::DumpFile( $failedfile, \@failed );

infof('[Finish the all]');
exit;

=head1 NAME

byakhee.pl - Website update checker written by modern Perl

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This application is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut
